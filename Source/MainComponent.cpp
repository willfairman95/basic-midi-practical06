/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    StringArray sa (MidiInput::getDevices());
    for (String s : sa)
        DBG(s);
    
    setSize (500, 400);
    
    //audio device manger
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    audioDeviceManager.setDefaultMidiOutput("SimpleSynth virtual input");
    
    
    noteSlider.setSliderStyle(Slider::SliderStyle::IncDecButtons);
    noteSlider.setRange (0, 127, 1);
    addAndMakeVisible(&noteSlider);
    
    addAndMakeVisible(&noteLabel);
    noteLabel.setText("Note", juce::NotificationType::dontSendNotification);
    
    messageLabel.setText("Message Type", juce::NotificationType::dontSendNotification);
    addAndMakeVisible(&messageLabel);

    messageType.addItem("Note", Note);
    messageType.addItem("Pitch", Pitch);
    addAndMakeVisible(&messageType);
    
    channelSlider.setSliderStyle(Slider::SliderStyle::IncDecButtons);
    channelSlider.setRange (1, 16, 1);
    addAndMakeVisible(&channelSlider);
    
    channelLabel.setText("Channel", juce::NotificationType::dontSendNotification);
    addAndMakeVisible(&channelLabel);
    
    velocitySlider.setSliderStyle(Slider::SliderStyle::IncDecButtons);
    velocitySlider.setRange (0, 127, 1);
    addAndMakeVisible(&velocitySlider);
    
    velocityLabel.setText("Velocity", juce::NotificationType::dontSendNotification);
    addAndMakeVisible(&velocityLabel);
    
    sendButton.setButtonText("Send");
    sendButton.addListener(this);
    addAndMakeVisible(&sendButton);
    
    
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    //midiLabel
    midiLabel.setBounds(20, 20, getWidth(), getHeight());
    
    messageLabel.setBounds(20, 10, getWidth()/5, getHeight()/10);
    messageType.setBounds(messageLabel.getX(), messageLabel.getBottom(), getWidth()/5, getHeight()/10);
    
    channelLabel.setBounds(messageLabel.getRight() + 10, 10, getWidth() / 5, getHeight()/10);
    channelSlider.setBounds(channelLabel.getX(), channelLabel.getBottom(), getWidth() / 10, getHeight()/10);
    
    noteLabel.setBounds(channelLabel.getRight(), 10, getWidth() / 5, getHeight()/10);
    noteSlider.setBounds(noteLabel.getX(), noteLabel.getBottom(), getWidth() / 10, getHeight()/10);
    
    velocityLabel.setBounds(noteLabel.getRight(), 10, getWidth() / 5, getHeight()/10);
    velocitySlider.setBounds(velocityLabel.getX(), noteLabel.getBottom(), getWidth() / 10, getHeight()/10);
    
    sendButton.setBounds(getWidth()/2, 150, getWidth()/5, getHeight()/10);

}

void MainComponent::handleIncomingMidiMessage(MidiInput*, const MidiMessage&)
{
    DBG("MIDI Message Recieved\n");
   
    
    //display label text
    String midiText;
    MidiMessage message;
    if (message.isNoteOnOrOff())
    {
        midiText << "NoteOn: Channel " << message.getChannel();
        midiText << ":Number" << message.getNoteNumber();
        midiText << ":Velocity" << message.getVelocity();
    }
    if (message.isProgramChange())
    {
        midiText << "Program number:" << message.getProgramChangeNumber();
    }
    if (message.isPitchWheel())
    {
        midiText << "Pitch:" << message.getPitchWheelValue();
    }
    if (message.isController())
    {
        midiText << "Control number:" << message.getControllerNumber() << " Value:" << message.getControllerValue();
    }
    
    audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(message);
    
    midiLabel.getTextValue() = midiText;
   
}

void MainComponent::buttonClicked (Button* button)
{
    if (button == &sendButton)
    {
        MidiMessage completeNote;
        if (messageType.getSelectedId() == Note)
        {
            DBG (channelSlider.getValue() << ":" << noteSlider.getValue() << ":" << (uint8)velocitySlider.getValue());
            
            completeNote = MidiMessage::noteOn(channelSlider.getValue(), noteSlider.getValue(), (uint8)velocitySlider.getValue());
                
        }
        else if (messageType.getSelectedId() == Pitch)
        {
            completeNote = MidiMessage::controllerEvent(channelSlider.getValue(), 1, noteSlider.getValue());
        }
        audioDeviceManager.getDefaultMidiOutput()->sendMessageNow(completeNote);
    
    }
}