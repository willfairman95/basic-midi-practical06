/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"


//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component,
                        public MidiInputCallback,
                        public Button::Listener
            
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();
    
    enum MessageType
    {
        Note = 1,
        Pitch
    };

    void resized() override;
    void handleIncomingMidiMessage (MidiInput*, const MidiMessage&);

    void buttonClicked (Button*) override;
    
private:
    Label midiLabel, noteLabel, messageLabel, channelLabel, velocityLabel;
    Slider noteSlider, channelSlider, velocitySlider;
    ComboBox messageType;
    TextButton sendButton;
    AudioDeviceManager audioDeviceManager;
    
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    
};


#endif  // MAINCOMPONENT_H_INCLUDED
